package go_lodestone

import (
	"errors"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

func (a *Api) Get(uri string) (*goquery.Document, error) {
	// TODO: sanity checking
	res, err := http.Get(a.baseUri + uri)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, errors.New("page did not load correctly")
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return nil, err
	}
	return doc, nil
}
