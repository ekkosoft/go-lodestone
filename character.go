package go_lodestone

import (
	"errors"
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"gitlab.com/ekkosoft/go-lodestone/endpoint"
)

func (ls *Lodestone) CharacterSearch(world string, name string, surname string) ([]*CharacterSearchResult, error) {
	res := make([]*CharacterSearchResult, 0)
	if len(world) < 1 || len(name) < 1 || len(surname) < 1 {
		return nil, errors.New("world, name, and surname cannot be empty")
	}
	world = strings.Title(strings.ToLower(world))
	d, err := ls.api.Get(fmt.Sprintf(endpoint.CharacterQuery, name, surname, world))
	if err != nil {
		return nil, fmt.Errorf("error getting page data: %v", err)
	}
	d.Find("a.entry__link").Each(func(i int, s *goquery.Selection) {
		href := s.AttrOr("href", "")
		id := strings.Replace(strings.Trim(href, "/"), "lodestone/character/", "", -1)
		name := strings.Split(s.Find("p.entry__name").First().Text(), " ")
		face := s.Find("div.entry__chara__face img").First().AttrOr("src", "")
		fc := s.Closest("div.entry").Find("a.entry__freecompany__link span").First().Text()
		res = append(res, &CharacterSearchResult{
			ID:      id,
			Name:    name[0],
			Surname: name[1],
			World: world,
			FacePortrait: face,
			FreeCompany: fc,
		})
	})
	return res, nil
}

func (ls *Lodestone) GetCharacterProfile(id string) (*CharacterProfile, error) {
	d, err := ls.api.Get(fmt.Sprintf(endpoint.CharacterProfile, id))
	if err != nil {
		return nil, fmt.Errorf("error getting page data: %v", err)
	}
	char := &CharacterProfile{ID: id}
	d.Find("div#character").Each(func(i int, s *goquery.Selection) {
		// Basic Data
		char.FacePortrait = s.Find("div.frame__chara__face img").First().AttrOr("src", "")
		char.Title = s.Find("p.frame__chara__title").First().Text()
		char.FullName = s.Find("p.frame__chara__name").First().Text()
		char.World = s.Find("p.frame__chara__world").First().Text()
		// Character Details
		s.Find("div.character__profile__data__detail div.character-block__box").Each(
			func(i int, cbb *goquery.Selection) {
			if i == 0 {
				data, err := cbb.Find("p.character-block__name").First().Html()
				if err == nil {
					x := strings.Split(data, "<br/>")
					char.Race = x[0]
					y := strings.Split(x[1], " / ")
					char.Clan = y[0]
					char.Gender = y[1]
				}
			}
			if i == 1 {
				char.Nameday = cbb.Find("p.character-block__birth").First().Text()
				char.Guardian = cbb.Find("p.character-block__name").First().Text()
			}
			if i == 2 {
				char.CityState = cbb.Find("p.character-block__name").First().Text()
			}
			if i == 3 {
				if cbb.Find("p.character-block__title").First().Text() == "Grand Company" {
					char.GrandCompany = cbb.Find("p.character-block__name").First().Text()
				} else {
					i++
				}
			}
			if i == 4 {
				char.FreeCompany = cbb.Find("div.character__freecompany__name h4 a").First().Text()
			}
		})
		// Full Portrait
		char.FullPortrait = s.Find("div.character__detail__image a").First().AttrOr("href", "")
		// Class Levels
		classes := &CharacterProfileClasses{}
		s.Find("div.character__level.clearfix").Each(
			func(x int, cl *goquery.Selection) {
				// Tanks and Healers
				if x == 0 {
					cl.Find("li").Each(func(y int, level *goquery.Selection) {
						if y == 0 { classes.PLD = level.Text() }
						if y == 1 { classes.WAR = level.Text() }
						if y == 2 { classes.DRK = level.Text() }
						if y == 3 { classes.GNB = level.Text() }
						if y == 4 { classes.WHM = level.Text() }
						if y == 5 { classes.SCH = level.Text() }
						if y == 6 { classes.AST = level.Text() }
					})
				}
				// DPS
				if x == 1 {
					cl.Find("li").Each(func(y int, level *goquery.Selection) {
						if y == 0 { classes.MNK = level.Text() }
						if y == 1 { classes.DRG = level.Text() }
						if y == 2 { classes.NIN = level.Text() }
						if y == 3 { classes.SAM = level.Text() }
						if y == 4 { classes.BRD = level.Text() }
						if y == 5 { classes.MCH = level.Text() }
						if y == 6 { classes.DNC = level.Text() }
						if y == 7 { classes.BLM = level.Text() }
						if y == 8 { classes.ACN = level.Text() }
						if y == 9 { classes.RDM = level.Text() }
						if y == 10 { classes.BLU = level.Text() }
					})
				}
				// DoH
				if x == 2 {
					cl.Find("li").Each(func(y int, level *goquery.Selection) {
						if y == 0 { classes.CRP = level.Text() }
						if y == 1 { classes.BSM = level.Text() }
						if y == 2 { classes.ARM = level.Text() }
						if y == 3 { classes.GSM = level.Text() }
						if y == 4 { classes.LTW = level.Text() }
						if y == 5 { classes.WVR = level.Text() }
						if y == 6 { classes.ALC = level.Text() }
						if y == 7 { classes.CUL = level.Text() }
					})
				}
				// DoL
				if x == 3 {
					cl.Find("li").Each(func(y int, level *goquery.Selection) {
						if y == 0 { classes.MIN = level.Text() }
						if y == 1 { classes.BTN = level.Text() }
						if y == 2 { classes.FSH = level.Text() }
					})
				}
			})
		char.Classes = classes
	})
	return char, nil
}