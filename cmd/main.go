package main

import (
	"fmt"
	"log"

	lodestone "gitlab.com/ekkosoft/go-lodestone"
)

func main() {
	api, err := lodestone.NewApi("na")
	if err != nil {
		log.Fatalf("error creating api: %s", err)
	}
	ls := lodestone.NewLodestone(api)
	res, err := ls.GetCharacterProfile("17086925")
	if err != nil {
		log.Fatalf("error finding character: %v", err)
	}
	fmt.Printf("profile: %+v\n", res)
	fmt.Printf("classes: %+v\n", res.Classes)
}
