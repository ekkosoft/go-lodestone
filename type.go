package go_lodestone

import "github.com/PuerkitoBio/goquery"

type ILodestone interface {
	CharacterSearch(world string, name string, surname string) ([]*CharacterSearchResult, error)
	GetCharacterProfile(id string) (*CharacterProfile, error)
}

type Lodestone struct {
	ILodestone
	api IApi
}

func NewLodestone(api *Api) *Lodestone {
	return &Lodestone{api: api}
}

type IApi interface {
	Get(uri string) (*goquery.Document, error)
}

type Api struct {
	IApi
	baseUri string
}

func NewApi(region string) (*Api, error) {
	// TODO: sanity checking
	return &Api{
		baseUri: "https://"+region+".finalfantasyxiv.com/lodestone/",
	}, nil
}

type CharacterSearchResult struct {
	ID string
	Name string
	Surname string
	World string
	FacePortrait string
	FreeCompany string
}

type CharacterProfile struct {
	ID string
	FacePortrait string
	Title string
	FullName string
	World string
	Race string
	Clan string
	Gender string
	Nameday string
	Guardian string
	CityState string
	GrandCompany string
	FreeCompany string
	FullPortrait string
	Classes *CharacterProfileClasses
}

type CharacterProfileClasses struct {
	PLD string
	WAR string
	DRK string
	GNB string

	WHM string
	SCH string
	AST string

	MNK string
	DRG string
	NIN string
	SAM string
	BRD string
	MCH string
	DNC string
	BLM string
	ACN string
	RDM string
	BLU string

	CRP string
	BSM string
	ARM string
	GSM string
	LTW string
	WVR string
	ALC string
	CUL string

	MIN string
	BTN string
	FSH string
}